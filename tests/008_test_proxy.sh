#!/bin/bash
set -x
# assume that
#   DIRECT connection to network is possible
#   PROXY at 127.0.0.1:80 handles HTTP requests but does not proxy (apache default config)

. tests/000_func

#test that env-var handling gives out appropriate error.
HTTP_PROXY=http://127.0.0.1 SOAP_USE_PROXY=on LC_ALL=C grep-result "environment variable http_proxy is unset and HTTP_PROXY is set" \
./apt-listbugs -C tests/008_apt-conf.null -y list apt-listbugs

HTTP_PROXY=http://127.0.0.1 SOAP_USE_PROXY=on  LC_ALL=C grep-result "environment variable http_proxy is unset and HTTP_PROXY is set" \
./apt-listbugs -C tests/008_apt-conf.direct1 -y list apt-listbugs

http_proxy=http://127.0.0.1 SOAP_USE_PROXY=off  LC_ALL=C grep-result "environment variable http_proxy is set and soap_use_proxy is not" \
./apt-listbugs -C tests/008_apt-conf.direct1 -y list apt-listbugs


#test that env var overrides config files.
http_proxy=http://127.0.0.1 soap_use_proxy=on fail ./apt-listbugs -C tests/008_apt-conf.null -y list apt-listbugs
http_proxy=http://127.0.0.1 soap_use_proxy=on fail ./apt-listbugs -C tests/008_apt-conf.direct1 -y list apt-listbugs

success ./apt-listbugs -C tests/008_apt-conf.null -y list apt-listbugs
fail ./apt-listbugs -C tests/008_apt-conf.proxy -y list apt-listbugs
success ./apt-listbugs -C tests/008_apt-conf.direct1 -y list apt-listbugs
success ./apt-listbugs -C tests/008_apt-conf.direct2 -y list apt-listbugs
success ./apt-listbugs -C tests/008_apt-conf.direct2 -y list apt-listbugs
fail ./apt-listbugs -C tests/008_apt-conf.proxy-bdo -y list apt-listbugs
fail ./apt-listbugs -C tests/008_apt-conf.378868 -y list apt-listbugs

echo 'complete'
exit 0
