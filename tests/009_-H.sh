#!/bin/bash
. tests/000_func

grep-result "SOAP::HTTPStreamError" ./apt-listbugs list  apt-listbugs -H merkel.debian.org -p 80 -d -n
grep-result "Errno::ECONNREFUSED" ./apt-listbugs list  apt-listbugs -H merkel.debian.org -p 81 -d -n
grep-result-not "SOAP::HTTPStreamError" ./apt-listbugs list  apt-listbugs -H bugs.debian.org -p 80 -y -d
exit 0
